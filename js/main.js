const openTrigger = document.getElementsByClassName('sidebar-trigger-open');
const closeTrigger = document.getElementsByClassName('sidebar-trigger-close');
const body = document.body;

openTrigger[0].addEventListener('click', function () {
    body.classList.add('sidebar-open');
});

closeTrigger[0].addEventListener('click', function () {
    body.classList.remove('sidebar-open');
});
